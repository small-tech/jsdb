/**
  Migrate a database from JSDB version 2-4 (JSDF 2) to JSDB version 5+ (JSDF 3).
*/
import fs from 'node:fs'
import path from 'node:path'
import JSDB from '../../index.js'

const commandlineArguments = process.argv
let databaseDirectory = '.'
if (commandlineArguments.length === 3) {
  databaseDirectory = commandlineArguments[2]
}

console.info('Migrating JSDB database from JSDF 2 to 3.\n')
console.info('Database: ', path.resolve(databaseDirectory), '\n')

let tableFileNames = fs.readdirSync(databaseDirectory)

console.info('Compiling list of custom object classes from tables:\n')

// Compile list of classes used in the database, if any,
// from the source code of the table files and construct
// barebones classes with the same name that conform to
// the JSDF 3 constructor requirement.

let classNames = []
let classes = []

tableFileNames.forEach(tableFileName => {
  console.info(' •', tableFileName)
  const tableSource = fs.readFileSync(path.join(databaseDirectory, tableFileName), 'utf-8')
  const classesInTable = [...tableSource.matchAll(/typeof (.+?) /g)].map(match => {return ({[match[1]]:
    class {
      constructor (parameters) {
        Object.assign(this, parameters)
      }
    }})[ match[1]] })
  classesInTable.forEach(klass => {
    const className = klass.name
    if (!classNames.includes(className)) {
      classNames.push(className)
      classes.push(klass)
    }
  })
})

console.info('\nOpening database and writing it out in JSDF 3 format.\n')

// Load the database into memory, passing the list of
// barebones classes so custom objects can be properly deserialised.
const db = JSDB.open(databaseDirectory, { classes })

console.info('\nDone.')

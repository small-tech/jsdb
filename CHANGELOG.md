# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [6.0.0] - 2024-06-18

No more symbols.

### Changed

  - Remove support for Symbols.

    Serialising symbols was never working properly and can’t work properly since we cannot differentiate between registered and unregistered symbols. (We were previously creating registered symbols during the serialisation process and, apparenty, doing so erroneously to boot by not quoting the symbol key. Hey, another reminder that 100% test coverage does not mean zero bugs!) In any case, this ‘feature’ was also causing crashes when `EventEmitter` subclasses were being persisted (e.g., when the `removeAllListeners()` method is called, which tries to set [`this[kShapeMode] = false`](https://github.com/nodejs/node/blob/main/lib/events.js#L762)).

    Symbol support should never have existed. Likely, no one is using or relying on it anyway but, since this is a potentially breaking change (in case someone is peristing objects with registered symbol keys in their parents’ basement in Finland), we have a major version bump. 

### Fixed

  - The distribution build and minified distribution build scripts are now functional again and all tests are now passing again for distribution builds. 

### Housekeeping

  - Update all development dependencies to latest versions.

## [5.1.3] - 2024-05-07

### Fixed

  - Deep proxies are no longer created for keys starting with an underscore (`_`).

    This was causing issues (e.g., `_events` in `EventEmitter` subclasses were still being persisted, resulting in a crash when the database was opened after an `EventEmitter` instance added a listener to its `_events` object as the `AsyncFunction` class was not provided to JSDB’s `open()` method).

## [5.1.2] - 2024-05-07

### Fixed

  - Plain object properties that start with an underscore are now properly ignored.

## [5.1.1] - 2024-05-06

### Changed

  - Optimise JSDF’s custom object serialisation by removing redundant return statement. This change is backwards compatible and should not affect your existing projects in any way. 

## [5.1.0] - 2024-05-02

### Changed

  - Attempting to load a database without providing all the custom classes persisted in it to your `JSDB.open()` call now throws an error instead of corrupting your database by falling back to using an untyped object.

  - Above change means the custom object instantiation code in your tables is different. However, this is not a breaking change. Any databases created in the last two days since the release of JSDB 5.0.0 will automatically be updated to the next syntax as they are compacted on database open. (If you have compaction turned off, the new behaviour will only apply to newly-persisted objects.

### Added

  - JSDF version 2 to 3 migration script (i.e., JSDB version 2-4 to 5 migration script). For usage instructions, please [see the readme](https://codeberg.org/small-tech/jsdb#version-2-to-3). 

## [5.0.1] - 2024-05-01

Mayday, mayday!

### Fixed

  - [#14](https://codeberg.org/small-tech/jsdb/issues/14) Crash if DataProxy getHandler() called on object with null prototype.

## [5.0.0] - 2024-04-30

This update brings with it JSDF version 3, with improved support for custom objects (with breaking changes).

### Changed

#### Breaking changes

  - Custom classes are expected to have a `constructor` that accepts a parameter object as its only argument. The custom class must assign properties on itself that mirror the properties passed in this parameter object.

  - The constructor of persisted custom classes is run when deserialising the object and must conform to the form outlined in the first point in order for the object to be properly deserialised.
  
  - Properties that begin with an underscore (`_`) are treated as private and ignored (note that due to the complexities of native private properties – i.e., `#thoseThatBeginWithAHash` – in JavaScript when interacting with proxied objects, it is recommended that you do not use native private properties in your custom classes).

#### Non-breaking changes

  - Custom classes can extend other classes (e.g., `EventEmitter`).

  - Custom objects have their methods ignored by default (instead of throwing for trying to persist a function)

  - Objects with null prototypes are supported. i.e., objects created with `Object.create(null, …)`.

## [4.0.0] - 2023-11-02

### Removed

__Breaking change.__

Removed proxied methods that were being injected on table objects (as well as, erroneously, on all items in a table):

  - `delete()`
  - `addEventListener()`
  - `removeListener()`

#### Details

DataProxy no longer injects virtual `delete()` method into all items in a table that proxy to the table’s `delete()` method.

This was both a feature and a bug.

It was a feature in that it was intended to be a helpful shorthand for deleting tables that should only have existed on the table instance itself. Even this was problematic in that you could have a custom collection class as your table and that collection class could have had a `delete()` method and it would not have been called (and any custom housekeeping implemented in that method would not have gotten called).

More distressingly, the proxy method was not just being added to the table object but to every element within it. So, for example, if you were storing custom classes that had a `delete()` method on them in your table and you called that method… well, boom!… your whole table would be deleted.

So, yeah, not great! :)

This release removes this “shorthand” so it is a breaking change in case you were relying on it.

For consistency (and because the pollution of all data items applied equally to them), the injected `addEventListener()` and `removeEventListener()` methods have also been removed. From now on, if you want to access the internal table methods, you must do so explicitly using the `__table__` property of table objects.

## [3.0.4] - 2023-09-18

### Fixed

  - Null and undefined values are correctly persisted. Setting object property to null no longer results in error (see [#2](https://codeberg.org/small-tech/jsdb/issues/2)).

  - Tests no longer crash on Windows due to path issue (see [#9](https://codeberg.org/small-tech/jsdb/issues/9)).

### Improved

  - Bring code coverage back to 100%.

  Thanks to [Kote Isaev](https://mastodon.online/@koteisaev) for his work in making this release possible.

## [3.0.3] - 2023-08-04

### Fixed

  - JSDB and JSTable defaults are now all applied when partial options argument is passed to the constructor. (See [#6](https://codeberg.org/small-tech/jsdb/issues/6)).

## [3.0.2] - 2023-05-22

### Fixed

  - Tightened up Date object check in data proxy so custom classes that include the string 'Date' in their names don’t trigger it.

## [3.0.1] - 2023-05-22

### Fixed

  - Calling methods on persisted Date objects read back into memory now works as it should. (https://codeberg.org/small-tech/jsdb/issues/5)

## [3.0.0] - 2023-04-28

__Breaking change:__ data is now evaluated in a custom context within a virtual machine. If you were placing custom classes in the global scope (`globalThis`) and relying on that to have data deserialised with the correct type at load time, you are now going to have to pass in the list of custom classes to use in the new `classes` property of the `options` object when opening the database.

(The previous behaviour was unintentional, undocumented, and should be considered a bug. Please update your global scope workarounds before upgrading to this major version.)

### Added

  - `classes` property to `options` object used in the `JSDB.open()` method.

### Fixed

  - Type information for custom objects being lost unless their class was available on the global scope during `JSDB.load()`.

## [2.1.0] - 2022-09-10

  - Expose `compactOnLoad` `JSTable` option on `JSDB.open()`
  - Espose `alwaysUseLineByLineLoads` `JSTable` option on `JSDB.open()`

## [2.0.7] (also backported to @cjs 1.2.3) - 2022-09-06

### Fixed

  - Fix escaping of string keys that contain a single quote. (#1)*
  
  * Note: Issue numbers have reset as the project has moved to Codeberg.

## [2.0.6] (also backported to @cjs 1.2.2) - 2022-01-01

### Fixed

  - Crash during copy of complex data types (e.g., Date) from one table to another. This was due to proxied data types not providing a bound reference to the toJSON() method of their targets when being serialised. (#14)

## [2.0.5] (also backported to @cjs 1.2.1) - 2021-12-29

### Fixed

  - Improve algorithm that decides when to quote or not quote object keys when serialising them to handle object/array key edge cases (like strings that start with a number or contain all digits and have left padding, etc.) (#13)

## [2.0.4] - 2021-07-30

### Improved

  - Minor: Refactored change in last release for clarity (“naming things is hard”). Removed log statement.

## [2.0.3] - 2021-07-30

### Fixed

  - Table load no longer crashes if data contains multiline string (#10)

## [2.0.2] - 2021-04-23

### Improved

  - Nothing. This is a superflous release because apparently [npm cannot intelligently handle publishing a fix for an earlier major version](https://stackoverflow.com/questions/24691314/npm-publish-patch-for-earlier-major-version).

## [2.0.1] - 2021-03-03

### Improved

  - npm package size reduced to 29.7kb (97.3kb unpacked) from 62.8kb (260.4kb unpacked) by specifying whitelist of included files in package file.
  - Reduced size of license file by not including the whole AGPL license text (that’s what URLs are for).

## [2.0.0] - 2021-03-03

### Breaking changes

  - Uses EcmaScript Modules (ESM). (Requires Node 14 or later.)
  - JSDF now only supports/serialises to ESM format.

### Changed

  - For regular/smaller data sets (under 500MB), JSDB now reads the file in synchronously and evals it, instead of using `require()`, as before. (I chose not to use a dynamic `import()` as it is asynchronous.)
  - For larger data sets, we’re now using an inlined version of `n-readlines`.
  - The module now has zero runtime dependencies.

### Added

  - 32KB distribution version (run `npm run build` and find it in `dist/index.js`).

## [1.1.5] - 2020-10-31

### Improved

  - Attempt to create a query on a non-array object now throws `TypeError`. (#12)

## [1.1.4] - 2020-10-29

### Fixed

  - Object keys containing non-alphanumeric characters are now properly supported. (#11)

## [1.1.3] - 2020-10-28

### Improved

Query security updates:

  - Fail faster on disallowed character detection.
  - Fail at function creation instead of code execution on syntax error by using function constructor instead of eval.
  - Add square brackets to disallowed characters. As far as I can see, [esoteric](http://www.businessinfo.co.uk/labs/talk/Nonalpha.pdf) [approaches](http://slides.com/sylvainpv/xchars-js/) to writing non-alphanumeric JavaScript were already being thwarted by disallowing the plus sign, semicolon, etc., but there’s no harm in removing these also as subscript syntax is powerful in JavaScript.
  - Add a few more tests.

## [1.1.2] - 2020-10-23

### Improved

  - Refactor query sanitisation code to make it safer and more maintainable; add tests.

## [1.1.1] - 2020-10-21

### Fixed

  - Make sanitisation regular expression less greedy (the sieve now catches injection attempts it was previously missing).
  - Boolean value support in queries now works properly (was previously being filtered by the sieve).

### Added

  - Missing test for boolean values in queries.

## [1.1.0] - 2020-10-21

### Added

  - Now sanitising queries to prevent arbitrary code execution via injection attacks.

## [1.0.0] - 2020-10-19

Initial release.

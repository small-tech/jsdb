// Run this after running index.js to see
// the behaviour when the Person class doesn’t exist.
import JSDB from '../../index.js'

class Person {
  constructor ({ name = 'Jane Doe' }={}) {
    this.name = name
  }
  introduceYourself () {
    console.log(`Hello, I’m ${this.name}.`)
  }
}

// Note we are NOT registering the class with JSDB.
// Even though the class exists within the local scope,
// JSDB will not be able to access it and type information
// will not exist in either the database and in memory.
const db = JSDB.open('db')
console.log(db.people[1])
